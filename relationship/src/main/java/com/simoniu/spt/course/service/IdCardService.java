package com.simoniu.spt.course.service;

import com.simoniu.spt.course.entity.IdCard;

public interface IdCardService {

    IdCard saveIdCard(IdCard card);
}
