package com.simoniu.spt.course.entity;

//班级类
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClassRoom implements Serializable,Cloneable {

    @Id
    @GeneratedValue(generator="mycid")
    @GenericGenerator(name="mycid",strategy="assigned")
    @Column(length = 4)
    private String cid; //班级编号
    private String cname; //班级的名称

}
