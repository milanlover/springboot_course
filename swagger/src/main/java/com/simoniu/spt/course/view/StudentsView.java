package com.simoniu.spt.course.view;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentsView implements Serializable,Cloneable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //使用数据库默认的主键生成策略
    private int sid; //主键
    @Column(length = 20)  //规定学生的姓名不超过20个字符
    private String sname; //姓名，默认是占用255个字符。
    @Column(length = 2)
    private String gender; //性别
    @Column(length = 32)
    private String birthday; //出生日期
    private String school; //所在学校
    private String major; //所学的专业

    private String cid; //班级编号
    private String cname; //班级的名称
}
