package com.simoniu.spt.course.repository;

import com.simoniu.spt.course.entity.Users;

public interface UsersRepository extends BaseRepository<Users,Integer>{

}
