package com.simoniu.spt.course.controller;

import com.simoniu.spt.course.entity.Users;
import com.simoniu.spt.course.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api("用户模块接口")
@RestController
@RequestMapping("users")
public class UsersController {


    //Redis缓存模板对象
    @Resource
    protected RedisTemplate redisTemplate;

    @RequestMapping("object") // 测试返回单个对象
    public Users returnSingleUsers(){
        Users user = new Users();
        user.setUid(100);
        user.setUsername("张三丰");
        user.setPassword("123456");
        return user;
    }


    //测试返回集合
    @RequestMapping("list")
    public List<Users> returnUsersList(){
        List<Users> usersList = new ArrayList<Users>();
        usersList.add(new Users(100,"zhangsan","123456"));
        usersList.add(new Users(101,"lisi","123456"));
        usersList.add(new Users(102,"wangwu","123456"));
        return usersList;
    }

    //测试返回map
    @RequestMapping("map")
    public Map<String,Object> returnMap(){
        Map<String,Object> map = new HashMap<String,Object>();
        //map.put("data",new Users(100,"zhangsan","123456"));
        map.put("data",null); //测试一下，json对null值的一个默认处理。
        map.put("msg","作者信息");
        map.put("code",200);
        return map;
    }

    @ApiOperation("用户登录接口")
    @GetMapping("auth")
    public R login(String username, String password){
        //判断一下，我们规定用户名必须是admin,密码必须是admin才能登录成功。
        if("admin".equals(username)&&"admin".equals(password)){
            Users loginUser = new Users(100,username,password);
            redisTemplate.opsForValue().setIfAbsent("loginSuccessUser",loginUser);

            return R.ok("loginSuccess",loginUser);
        }else{
            return R.fail("loginFailure");
        }
    }

    @ApiOperation("检查是否登录接口")
    @GetMapping("isLogin")
    public R isLogin(){
        //判断一下，我们规定用户名必须是admin,密码必须是admin才能登录成功。
        if(redisTemplate.opsForValue().get("loginSuccessUser")!=null){
            Users loginUser = (Users) redisTemplate.opsForValue().get("loginSuccessUser");
            return R.ok("isLoginSuccess",loginUser);
        }else{
            return R.fail("isLoginFail");
        }
    }

    @ApiOperation("用户注册接口")
    @PostMapping("/")
    public R reg(@RequestBody Users user){
        System.out.println("注册用户对象");
        System.out.println(user);

        return R.ok("regSuccess");
    }






}

