package com.simoniu.spt.course.controller;

import com.simoniu.spt.course.entity.Users;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("users")
public class UsersController {

    @RequestMapping("object") // 测试返回单个对象
    public Users returnSingleUsers(){
        Users user = new Users();
        user.setUid(100);
        user.setUsername("张三丰");
        user.setPassword("123456");
        return user;
    }


    //测试返回集合
    @RequestMapping("list")
    public List<Users> returnUsersList(){
        List<Users> usersList = new ArrayList<Users>();
        usersList.add(new Users(100,"zhangsan","123456"));
        usersList.add(new Users(101,"lisi","123456"));
        usersList.add(new Users(102,"wangwu","123456"));
        return usersList;
    }

    //测试返回map
    @RequestMapping("map")
    public Map<String,Object> returnMap(){
        Map<String,Object> map = new HashMap<String,Object>();
        //map.put("data",new Users(100,"zhangsan","123456"));
        map.put("data",null); //测试一下，json对null值的一个默认处理。
        map.put("msg","作者信息");
        map.put("code",200);
        return map;
    }


}

