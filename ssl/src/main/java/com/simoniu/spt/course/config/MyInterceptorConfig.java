package com.simoniu.spt.course.config;

import com.simoniu.spt.course.interceptor.MyInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MyInterceptorConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // /** 表示拦截所有动作。
        registry.addInterceptor(new MyInterceptor()).addPathPatterns("/users/*");
    }

}
