package com.simoniu.spt.course.service;

import com.simoniu.spt.course.entity.ClassRoom;

public interface ClassRoomService {

    ClassRoom saveClassRoom(ClassRoom classRoom);
}
