package com.simoniu.spt.course.repository;

import com.simoniu.spt.course.entity.Teachers;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeachersRepository extends JpaRepository<Teachers,String> {
}
