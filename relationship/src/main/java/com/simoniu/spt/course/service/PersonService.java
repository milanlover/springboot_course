package com.simoniu.spt.course.service;

import com.simoniu.spt.course.entity.Person;

public interface PersonService {

    Person savePerson(Person person);
}
