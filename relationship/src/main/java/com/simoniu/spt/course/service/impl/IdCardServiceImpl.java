package com.simoniu.spt.course.service.impl;

import com.simoniu.spt.course.entity.IdCard;
import com.simoniu.spt.course.repository.IdCardRepository;
import com.simoniu.spt.course.service.IdCardService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class IdCardServiceImpl implements IdCardService {

    @Resource
    private IdCardRepository idCardRepository;

    @Override
    public IdCard saveIdCard(IdCard card) {
        return idCardRepository.save(card);
    }
}
