package com.simoniu.spt.course.service;

import com.simoniu.spt.course.entity.Users;
import com.simoniu.spt.course.mapper.UsersMapper;
import com.simoniu.spt.course.repository.UsersRepository;

public interface UsersService extends BaseService<Users,Integer, UsersRepository, UsersMapper>{

}
