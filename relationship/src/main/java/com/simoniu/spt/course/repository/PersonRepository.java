package com.simoniu.spt.course.repository;

import com.simoniu.spt.course.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person,Integer> {

}
