package com.simoniu.spt.course.service.impl;

import com.simoniu.spt.course.entity.Students;
import com.simoniu.spt.course.repository.StudentsRepository;
import com.simoniu.spt.course.service.StudentsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class StudentsServiceImpl implements StudentsService {
    @Resource
    private StudentsRepository studentsRepository;

    @Override
    public Students save(Students s) {
        return studentsRepository.save(s);
    }

    @Override
    public Students queryStudentsBySid(Integer sid) {
        return studentsRepository.getOne(sid);
    }

    @Override
    public void delete(int sid) {
        studentsRepository.deleteById(sid);
    }

    @Override
    public Students update(Students s) {
        return studentsRepository.save(s);
    }

    @Override
    public List<Students> queryAllStudents() {
        return studentsRepository.findAll();
    }
}
