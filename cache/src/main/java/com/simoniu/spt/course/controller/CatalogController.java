package com.simoniu.spt.course.controller;

import com.simoniu.spt.course.entity.Catalog;
import com.simoniu.spt.course.service.CatalogService;
import com.simoniu.spt.course.util.R;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/test")
public class CatalogController extends BaseController{

    @Resource
    private CatalogService catalogService;

    @GetMapping("catalog")
    @Cacheable(cacheNames="allCatalogList",key = "#root.method.name")
    public R getAllCatalogList(){
        System.out.println("TestRedisCacheController=>getAllCatalogList()....");
        List<Catalog> catalogList = new ArrayList<Catalog>();
        try{
            catalogList = catalogService.queryAllCatalogList();
            redisUtil.set("allCatalogList",catalogList);
            return R.ok("查询板块列表缓存成功！",catalogList);
        }catch(Exception ex){
            ex.printStackTrace();
            return R.error("查询板块列表缓存出现异常！");
        }
    }

    @PutMapping("catalog")
    @CachePut(cacheNames="allCatalogList",key = "#root.method.name")
    public R updateAllCatalogList(){
        System.out.println("TestRedisCacheController=>updateAllCatalogList()....");
        List<Catalog> catalogList = new ArrayList<Catalog>();
        try{
            catalogList = catalogService.queryAllCatalogList();
            redisUtil.set("allCatalogList",catalogList);
            return R.ok("更新板块列表缓存成功！",catalogList);
        }catch(Exception ex){
            ex.printStackTrace();
            return R.error("更新板块列表缓存出现异常！");
        }
    }

    @DeleteMapping("catalog")
    @CacheEvict(cacheNames="allCatalogList",key = "'getAllCatalogList'")
    public R deleteAllCatalogList(){
        System.out.println("TestRedisCacheController=>deleteAllCatalogList()....");
        try{
            redisUtil.remove("allCatalogList");
            return R.ok("删除板块列表缓存成功！");
        }catch(Exception ex){
            ex.printStackTrace();
            return R.error("删除板块列表缓存出现异常！");
        }
    }

}
