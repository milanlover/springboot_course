package com.simoniu.spt.course.config;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;
import org.apache.ibatis.reflection.MetaObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import java.util.Date;

@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(MyMetaObjectHandler.class);

    @Override
    public void insertFill(MetaObject metaObject) {

        LOGGER.info("start insert fill ....");
        //2022-
        this.setFieldValByName("createTime", DateUtil.now(), metaObject);
        this.setFieldValByName("modifyTime", DateUtil.now(), metaObject);
        this.setFieldValByName("flag", false, metaObject);
        //元对象处理器接口添加version的insert默认值
        this.setFieldValByName("version", 0, metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        LOGGER.info("start update fill ....");
        this.setFieldValByName("modifyTime", DateUtil.now(), metaObject);

    }

}
