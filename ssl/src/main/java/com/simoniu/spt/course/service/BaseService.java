package com.simoniu.spt.course.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.simoniu.spt.course.entity.BaseEntity;
import com.simoniu.spt.course.repository.BaseRepository;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @param <T> 表示具体的类型
 * @param <I> 表示该类型的主键类型
 * @param <R> 表示该类型的Repository层的实现类型。
 */

public interface BaseService<T extends BaseEntity, I extends Serializable, R extends BaseRepository<T, I>,M extends BaseMapper> {

    //1.下面是封装mybatisplus的BaseMapper里的方法,以后以mybatis结尾的都是调用mybatisplus的实现。

    int insertWithMybatis(T obj);

    int deleteByIdWithMybatis(I id);

    int updateByIdWithMybatis(T obj);

    T selectByIdWithMybatis(I id);

    List<T> selectListWithMybatis(Wrapper<T> wrapper);

    //针对所有的服务层都那些公共方法呢。
    //1.查询所有
    List<T> findAll();

    List<T> findAll(Example<T> example);

    //2.根据主键查询单个对象。
    T getOne(I id);

    //根据条件查询单个对象
    T getOne(Specification<T> var1);

    //3.查询分页记录。
    Page<T> findAllByPager(Pageable pageable);

    //4.添加单个对象
    T save(T obj);

    //5.修改单个对象
    T update(T obj);

    //6.根据主键删除单个对象。
    void deleteById(I id);

    //7.批量添加
    List<T> saveList(List<T> list);

    //8.批量删除
    void batchDelete(List<T> list);

}
