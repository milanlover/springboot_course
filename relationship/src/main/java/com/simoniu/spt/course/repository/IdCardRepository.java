package com.simoniu.spt.course.repository;

import com.simoniu.spt.course.entity.IdCard;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IdCardRepository extends JpaRepository<IdCard,String> {

}
