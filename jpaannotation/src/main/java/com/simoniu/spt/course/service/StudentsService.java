package com.simoniu.spt.course.service;

import com.simoniu.spt.course.entity.Students;

import java.util.List;

public interface StudentsService {

    Students save(Students s); //保存一个学生对象；
    Students queryStudentsBySid(Integer sid); //查询单个学生
    void delete(int sid);
    Students update(Students s);
    List<Students> queryAllStudents(); //查询所有学生
}
