package com.simoniu.spt.course.service;

import com.simoniu.spt.course.entity.Students;
import com.simoniu.spt.course.view.StudentsView;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public interface StudentsService {

    Students save(Students s); //保存一个学生对象；
    Students queryStudentsBySid(Integer sid); //查询单个学生
    void delete(int sid);
    Students update(Students s);
    List<Students> queryAllStudents(); //查询所有学生


    //查询学生的姓名和性别
    List<Students> findAllBySnameAndGender(String sname,String gender);
    //根据学生的出生日期进行排序
    List<Students> findAllByGenderOderByBirthdayDesc(String gender);

    //查询学生的视图，这个视图包含有班级的信息。
    List<StudentsView> queryStudentsView();

    //查询指定学生的视图
    StudentsView queryStudentsViewBySid(int sid);

    //分页查询
    Page<Students> findAllByPager(Pageable pageable);

    //使用Criteria 查询
    List<Students> findAll(Specification<Students> var1);
}
