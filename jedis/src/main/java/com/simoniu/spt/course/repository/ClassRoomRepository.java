package com.simoniu.spt.course.repository;

import com.simoniu.spt.course.entity.ClassRoom;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClassRoomRepository extends JpaRepository<ClassRoom,String> {

}
