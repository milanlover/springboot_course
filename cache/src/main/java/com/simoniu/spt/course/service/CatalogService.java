package com.simoniu.spt.course.service;

import com.simoniu.spt.course.entity.Catalog;
import com.simoniu.spt.course.repository.CatalogRepository;
import com.simoniu.spt.course.service.BaseService;

import java.util.List;

public interface CatalogService extends BaseService<Catalog,Integer, CatalogRepository> {
    List<Catalog> queryAllCatalogList();
}
