package com.simoniu.spt.course.service.impl;

import com.simoniu.spt.course.entity.ClassRoom;
import com.simoniu.spt.course.repository.ClassRoomRepository;
import com.simoniu.spt.course.service.ClassRoomService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class ClassRoomServiceImpl implements ClassRoomService {

    @Resource
    private ClassRoomRepository classRoomRepository;

    @Override
    public ClassRoom saveClassRoom(ClassRoom classRoom) {
        return classRoomRepository.save(classRoom);
    }
}
