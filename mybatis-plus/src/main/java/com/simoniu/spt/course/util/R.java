package com.simoniu.spt.course.util;

import lombok.Data;
import java.io.Serializable;

/**
 * @Description: 统一返回的结果类
 * @Author: StarSea99
 * @Date: 2020-09-27 22:55
 */
@Data
public class R implements Serializable,Cloneable {

    private Integer code;
    private String message;
    private Object data; //Map

    //把构造方法私有
    private R() {
    }

    //成功静态方法
    public static R ok() {
        R r = new R();
        r.setCode(ResultCode.SUCCESS);
        r.setMessage("成功");
        return r;
    }

    public static R ok(String msg) {
        R r = R.ok();
        r.setMessage(msg);
        return r;
    }

    public static R ok(String msg, Object data) {
        R r = R.ok();
        r.setMessage(msg);
        r.setData(data);
        return r;
    }

    //失败静态方法
    public static R error() {
        R r = new R();
        r.setCode(ResultCode.ERROR);
        r.setMessage("错误");
        return r;
    }

    public static R error(String msg) {
        R r = R.error();
        r.setMessage(msg);
        return r;
    }

    public static R fail() {
        R r = new R();
        r.setMessage("失败");
        r.setCode(ResultCode.FAIL);
        return r;
    }

    public static R fail(String msg) {
        R r = R.fail();
        r.setMessage(msg);
        return r;
    }
}
