package com.simoniu.spt.course.service;

import com.simoniu.spt.course.entity.ClassRoom;
import com.simoniu.spt.course.entity.Students;
import com.simoniu.spt.course.entity.Teachers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RunWith(SpringRunner.class)//指定Junit4使用Spring提供的测试环境
@SpringBootTest
public class StudentsServiceTest {


    @Resource
    private StudentsService studentsService;

    @Resource
    private ClassRoomService classRoomService;

    @Resource
    private TeachersService teachersService;

    //测试保存单个学生对象
    @Test
    public void testSaveStudents(){
        Students s = new Students();
        s.setSname("令狐冲");
        s.setGender("男");
        s.setSchool("华山思过崖大学");
        s.setDate("2010-10-10");
        s.setMajor("独孤九剑");
        s.setStatus(true);
        s = studentsService.save(s);
        System.out.println(s);
    }

    @Test
    public void testQueryStudentsBySid(){
        //查询出东方不败。
        Students s = studentsService.queryStudentsBySid(1);
        System.out.println(s);
    }

    //更新学生资料
    @Test
    public void testUpdateStudents(){
        //把东方不败的性别改为‘男’
        Students s = studentsService.queryStudentsBySid(1);
        s.setGender("男");
        studentsService.update(s);
        System.out.println(s);
    }

    //查询所有学生
    @Test
    public void testQueryAllStudents(){
        List<Students> studentsList = studentsService.queryAllStudents();
        for(Students s: studentsList){
            System.out.println(s);
        }
    }

    //删除学生
    @Test
    public void testDeleteStudentsBySid(){
        studentsService.delete(2);
    }

    @Test
    //测试多对一的关联关系，多个学生对应一个班级，一个班级对应多个学生
    public void testSaveStudentsAndClassRoom(){
        //先创建一个班级
        ClassRoom classRoom = new ClassRoom();
        classRoom.setCid("C001");
        classRoom.setCname("软件工程1班");

        //先保存班级资料
        classRoomService.saveClassRoom(classRoom);

        //创建三个学生对象，让他们都属于同一个班级。
        Students s1 = new Students();
        s1.setSchool("武汉大学");
        s1.setMajor("软件工程");
        s1.setSname("张三");
        s1.setGender("男");
        s1.setDate("1999-10-01");
        s1.setClassRoom(classRoom);

        Students s2 = new Students();
        s2.setSchool("武汉大学");
        s2.setMajor("软件工程");
        s2.setSname("李四");
        s2.setGender("女");
        s2.setDate("1999-09-01");
        s2.setClassRoom(classRoom);

        Students s3 = new Students();
        s3.setSchool("武汉大学");
        s3.setMajor("软件工程");
        s3.setSname("王五");
        s3.setGender("男");
        s3.setDate("1999-12-01");
        s3.setClassRoom(classRoom);
        //保存三个学生资料
        studentsService.save(s1);
        studentsService.save(s2);
        studentsService.save(s3);
    }

    @Test
    public void testSaveStudentsAndTeachers() {
        //先创建一个班级
        ClassRoom classRoom = new ClassRoom();
        classRoom.setCid("C002");
        classRoom.setCname("计算机科学与技术1班");

        //先保存班级
        classRoomService.saveClassRoom(classRoom);

        Students s1 = new Students();
        s1.setSchool("武汉大学");
        s1.setMajor("软件工程");
        s1.setSname("张三三");
        s1.setGender("男");
        s1.setDate("1999-10-01");
        s1.setClassRoom(classRoom);

        Students s2 = new Students();
        s2.setSchool("武汉大学");
        s2.setMajor("软件工程");
        s2.setSname("李四四");
        s2.setGender("女");
        s2.setDate("1999-09-01");
        s2.setClassRoom(classRoom);

        Students s3 = new Students();
        s3.setSchool("武汉大学");
        s3.setMajor("软件工程");
        s3.setSname("王五五");
        s3.setGender("男");
        s3.setDate("1999-12-01");
        s3.setClassRoom(classRoom);

        //创建三个教师对象
        Teachers t1 = new Teachers();
        t1.setTid("T001");
        t1.setTname("陈老师");

        Teachers t2 = new Teachers();
        t2.setTid("T002");
        t2.setTname("刘老师");

        Teachers t3 = new Teachers();
        t3.setTid("T003");
        t3.setTname("赵老师");

        //保存教师
        teachersService.saveTeachers(t1);
        teachersService.saveTeachers(t2);
        teachersService.saveTeachers(t3);

        Set<Teachers> set1 = new HashSet<Teachers>();
        set1.add(t1);
        set1.add(t2);
        set1.add(t3);

        Set<Teachers> set2 = new HashSet<Teachers>();
        set2.add(t1);
        set2.add(t2);

        Set<Teachers> set3 = new HashSet<Teachers>();

        set3.add(t2);
        set3.add(t3);
        //设置三个学生的教师集合
        s1.setTeachers(set1);
        s2.setTeachers(set2);
        s3.setTeachers(set3);

        //保存学生
        studentsService.save(s1);
        studentsService.save(s2);
        studentsService.save(s3);
    }
}
