package com.simoniu.spt.course.util;

/**
 * @Description: 状态码类
 * @Author: StarSea99
 * @Date: 2020-09-27 22:53
 */
public interface ResultCode {
    public static Integer SUCCESS = 200;//成功
    public static Integer ERROR = 500;//错误
    public static Integer FAIL = 400; //失败
}
