package com.simoniu.spt.course.view;

public interface  StudentsViewInterface {

    Integer getSid();
    String getSname();
    String getGender();
    String getBirthday();
    String getSchool();
    String getMajor();
    String getCid();
    String getCname();
}
