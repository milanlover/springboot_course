package com.simoniu.spt.course.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
//身份证类,将来不会映射为一张身份证表。
public class IdCard implements Serializable,Cloneable {
    @Id
    @Column(length = 18) //中国的身份证号码是18位的。
    private String pid; //身份证号码
    private String province;//省份

}
