package com.simoniu.spt.course.service;

import com.simoniu.spt.course.entity.Students;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringRunner.class)//指定Junit4使用Spring提供的测试环境
@SpringBootTest
public class StudentsServiceTest {


    @Resource
    private StudentsService studentsService;

    //测试保存单个学生对象
    @Test
    public void testSaveStudents(){
        Students s = new Students();
        s.setSname("令狐冲");
        s.setGender("男");
        s.setSchool("华山思过崖大学");
        s.setDate("2010-10-10");
        s.setMajor("独孤九剑");
        s.setStatus(true);
        s = studentsService.save(s);
        System.out.println(s);
    }

    @Test
    public void testQueryStudentsBySid(){
        //查询出东方不败。
        Students s = studentsService.queryStudentsBySid(1);
        System.out.println(s);
    }

    //更新学生资料
    @Test
    public void testUpdateStudents(){
        //把东方不败的性别改为‘男’
        Students s = studentsService.queryStudentsBySid(1);
        s.setGender("男");
        studentsService.update(s);
        System.out.println(s);
    }

    //查询所有学生
    @Test
    public void testQueryAllStudents(){
        List<Students> studentsList = studentsService.queryAllStudents();
        for(Students s: studentsList){
            System.out.println(s);
        }
    }

    //删除学生
    @Test
    public void testDeleteStudentsBySid(){
        studentsService.delete(2);
    }
}
