package com.simoniu.spt.course.service;

import com.simoniu.spt.course.entity.Users;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UsersServiceTest {

    @Resource
    private UsersService usersService;

    @Test
    public void testInsertWithMybatis(){
        Users u = new Users();
        u.setUsername("宋小宝爸爸");
        u.setPassword("123456");
        usersService.insertWithMybatis(u);
    }

    @Test
    public void testDeleteByIdWithMybatis(){
        usersService.deleteByIdWithMybatis(1);
    }

    @Test
    public void testUpdateByIdWithMybatis(){
        Users u = usersService.selectByIdWithMybatis(1);
        u.setPassword("6666666");
        usersService.updateByIdWithMybatis(u);
    }

    @Test
    public void testSave(){
        Users u = new Users();
        u.setUsername("张无忌");
        u.setPassword("654321");
        usersService.save(u);
    }

    @Test
    public void testUpdate(){
        Users u = usersService.getOne(2);
        u.setPassword("999999");
        usersService.update(u);
    }

    @Test
    public void testDeleteById(){
        usersService.deleteById(2);
    }
}
