package com.simoniu.spt.course.service.impl;

import com.simoniu.spt.course.entity.Users;
import com.simoniu.spt.course.mapper.UsersMapper;
import com.simoniu.spt.course.repository.UsersRepository;
import com.simoniu.spt.course.service.UsersService;
import org.springframework.stereotype.Service;

@Service
public class UsersServiceImpl extends BaseServiceImpl<Users,Integer, UsersRepository, UsersMapper> implements UsersService {

}
