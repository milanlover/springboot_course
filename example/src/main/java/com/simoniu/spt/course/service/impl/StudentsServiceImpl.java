package com.simoniu.spt.course.service.impl;

import com.simoniu.spt.course.entity.Students;
import com.simoniu.spt.course.repository.StudentsRepository;
import com.simoniu.spt.course.service.StudentsService;
import com.simoniu.spt.course.view.StudentsView;
import com.simoniu.spt.course.view.StudentsViewInterface;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class StudentsServiceImpl implements StudentsService {
    @Resource
    private StudentsRepository studentsRepository;

    @Override
    public Students save(Students s) {
        return studentsRepository.save(s);
    }

    @Override
    public Students queryStudentsBySid(Integer sid) {
        return studentsRepository.getOne(sid);
    }

    @Override
    public void delete(int sid) {
        studentsRepository.deleteById(sid);
    }

    @Override
    public Students update(Students s) {
        return studentsRepository.save(s);
    }

    @Override
    public List<Students> queryAllStudents() {
        return studentsRepository.findAll();
    }

    @Override
    public List<Students> findAllBySnameAndGender(String sname,String gender) {
        return studentsRepository.findAllBySnameAndGender(sname,gender);
    }

    @Override
    public List<Students> findAllByGenderOderByBirthdayDesc(String gender) {
        return studentsRepository.findByGenderOrderByBirthdayDesc(gender);
    }

    @Override
    public List<StudentsView> queryStudentsView() {

        List<StudentsView> studentsViewList = new ArrayList<StudentsView>();
        List<StudentsViewInterface> studentsViewInterfaceList = studentsRepository.queryStudentsView();
        for(StudentsViewInterface svf: studentsViewInterfaceList){
            StudentsView sv = new StudentsView();
            sv.setSid(svf.getSid());
            sv.setCname(svf.getCname());
            sv.setCid(svf.getCid());
            sv.setSname(svf.getSname());
            sv.setSchool(svf.getSchool());
            sv.setMajor(svf.getMajor());
            sv.setBirthday(svf.getBirthday());
            sv.setGender(svf.getGender());
            studentsViewList.add(sv);
        }
        return studentsViewList;
    }

    @Override
    public StudentsView queryStudentsViewBySid(int sid) {

        StudentsViewInterface svf = studentsRepository.queryStudentsViewBySid(sid);
        StudentsView sv = new StudentsView();
        sv.setSid(svf.getSid());
        sv.setCname(svf.getCname());
        sv.setCid(svf.getCid());
        sv.setSname(svf.getSname());
        sv.setSchool(svf.getSchool());
        sv.setMajor(svf.getMajor());
        sv.setBirthday(svf.getBirthday());
        sv.setGender(svf.getGender());
        return sv;
    }

    @Override
    public Page<Students> findAllByPager(Pageable pageable) {
        return studentsRepository.findAll(pageable);
    }

    @Override
    public List<Students> findAll(Specification<Students> var1) {
        return studentsRepository.findAll(var1);
    }

    @Override
    public List<Students> findAll(Example<Students> example) {
        return studentsRepository.findAll(example);
    }
}
