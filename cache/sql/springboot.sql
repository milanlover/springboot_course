/*
 Navicat Premium Data Transfer

 Source Server         : MySQL8.0.21
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : localhost:3306
 Source Schema         : springboot

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 07/08/2022 18:23:43
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for catalog
-- ----------------------------
DROP TABLE IF EXISTS `catalog`;
CREATE TABLE `catalog`  (
  `cid` int(0) NOT NULL AUTO_INCREMENT,
  `create_time` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `flag` tinyint(1) NULL DEFAULT 0,
  `modify_time` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `version` int(0) NULL DEFAULT NULL,
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `cname` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `pid` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`cid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of catalog
-- ----------------------------
INSERT INTO `catalog` VALUES (1, '2019-05-12 10:52:10', 0, NULL, 0, 'c', 'C/C++', 'top');
INSERT INTO `catalog` VALUES (2, '2019-05-12 10:53:18', 0, NULL, 0, 'java', 'Java/框架', 'top');
INSERT INTO `catalog` VALUES (3, '2019-05-12 10:53:18', 0, NULL, 0, 'h5', '前端', 'top');
INSERT INTO `catalog` VALUES (4, '2019-06-28 09:34:24', 0, NULL, 0, 'database', '数据库', 'top');
INSERT INTO `catalog` VALUES (5, '2019-05-12 10:53:18', 0, NULL, 0, 'linux', 'Linux', 'top');
INSERT INTO `catalog` VALUES (6, '2019-05-12 10:53:18', 0, NULL, 0, 'app', 'App/小程序', 'top');
INSERT INTO `catalog` VALUES (7, '2019-05-12 10:53:18', 0, NULL, 0, 'alg', '数据结构/算法', 'top');
INSERT INTO `catalog` VALUES (8, '2019-05-12 10:53:18', 0, NULL, 0, 'bigdata', '大数据', 'top');
INSERT INTO `catalog` VALUES (9, '2019-05-12 10:53:18', 0, NULL, 0, 'block', '区块链', 'top');
INSERT INTO `catalog` VALUES (10, '2019-05-12 10:53:18', 0, NULL, 0, 'python', 'Python/AI', 'top');
INSERT INTO `catalog` VALUES (11, '2019-05-12 10:53:18', 0, NULL, 0, 'net', '网络安全', 'top');
INSERT INTO `catalog` VALUES (12, '2019-05-12 10:53:18', 0, NULL, 0, 'wiki', '计算机理论', 'top');
INSERT INTO `catalog` VALUES (13, '2019-05-12 10:53:18', 0, NULL, 0, 'interview', '面试', 'top');
INSERT INTO `catalog` VALUES (14, '2019-05-12 10:53:18', 0, NULL, 0, 'ui', '平面888', 'top');

-- ----------------------------
-- Table structure for class_room
-- ----------------------------
DROP TABLE IF EXISTS `class_room`;
CREATE TABLE `class_room`  (
  `cid` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `cname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`cid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of class_room
-- ----------------------------
INSERT INTO `class_room` VALUES ('C001', '软件工程1班');
INSERT INTO `class_room` VALUES ('C002', '计算机科学与技术1班');

-- ----------------------------
-- Table structure for id_card
-- ----------------------------
DROP TABLE IF EXISTS `id_card`;
CREATE TABLE `id_card`  (
  `pid` varchar(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `province` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`pid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of id_card
-- ----------------------------
INSERT INTO `id_card` VALUES ('610103199910103657', '陕西省');

-- ----------------------------
-- Table structure for person
-- ----------------------------
DROP TABLE IF EXISTS `person`;
CREATE TABLE `person`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `gender` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `pid` varchar(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FKgailjlrmtkngfr1i3u93p67kv`(`pid`) USING BTREE,
  CONSTRAINT `FKgailjlrmtkngfr1i3u93p67kv` FOREIGN KEY (`pid`) REFERENCES `id_card` (`pid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of person
-- ----------------------------
INSERT INTO `person` VALUES (1, '男', '令狐冲', '610103199910103657');

-- ----------------------------
-- Table structure for students
-- ----------------------------
DROP TABLE IF EXISTS `students`;
CREATE TABLE `students`  (
  `sid` int(0) NOT NULL AUTO_INCREMENT,
  `birthday` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `gender` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `major` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `school` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `sname` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `status` bit(1) NOT NULL,
  `cid` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `create_time` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `flag` tinyint(1) NULL DEFAULT 0,
  `modify_time` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `version` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`sid`) USING BTREE,
  INDEX `FK2n4sqohtyc14idx8f1wrnk05p`(`cid`) USING BTREE,
  CONSTRAINT `FK2n4sqohtyc14idx8f1wrnk05p` FOREIGN KEY (`cid`) REFERENCES `class_room` (`cid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of students
-- ----------------------------
INSERT INTO `students` VALUES (1, '2000-10-10', '男', '葵花宝典', '黑木崖大学', '东方不败', b'1', NULL, NULL, 0, NULL, NULL);
INSERT INTO `students` VALUES (2, '2010-10-10', '男', '独孤九剑', '华山思过崖大学', '令狐冲', b'1', NULL, NULL, 0, NULL, NULL);
INSERT INTO `students` VALUES (3, '1999-10-01', '男', '软件工程', '武汉大学', '张三', b'0', 'C001', NULL, 0, NULL, NULL);
INSERT INTO `students` VALUES (4, '1999-09-01', '女', '软件工程', '武汉大学', '李四', b'0', 'C001', NULL, 0, NULL, NULL);
INSERT INTO `students` VALUES (5, '1999-12-01', '男', '软件工程', '武汉大学', '王五', b'0', 'C001', NULL, 0, NULL, NULL);
INSERT INTO `students` VALUES (6, '1999-10-01', '男', '软件工程', '武汉大学', '张三三', b'0', 'C002', NULL, 0, NULL, NULL);
INSERT INTO `students` VALUES (7, '1999-09-01', '女', '软件工程', '武汉大学', '李四四', b'0', 'C002', NULL, 0, NULL, NULL);
INSERT INTO `students` VALUES (8, '1999-12-01', '男', '软件工程', '武汉大学', '王五五', b'0', 'C002', NULL, 0, NULL, NULL);
INSERT INTO `students` VALUES (9, '2000-10-10', '男', '网络工程', '华中科技大学', '张三', b'1', NULL, NULL, 0, NULL, NULL);
INSERT INTO `students` VALUES (10, '1999-10-01', '男', '蛤蟆功', '石河子大学', '欧阳锋', b'1', NULL, '2022-08-06 23:19:56', 0, NULL, NULL);
INSERT INTO `students` VALUES (11, '1999-10-01', '男', '一阳指', '大理学院', '南帝', b'1', NULL, '2022-08-06 23:19:56', 0, NULL, NULL);
INSERT INTO `students` VALUES (12, '1999-10-01', '男', '降龙十八掌', '乞丐', '洪七公', b'1', NULL, '2022-08-06 23:19:56', 0, NULL, NULL);

-- ----------------------------
-- Table structure for teachers
-- ----------------------------
DROP TABLE IF EXISTS `teachers`;
CREATE TABLE `teachers`  (
  `tid` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `tname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`tid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of teachers
-- ----------------------------
INSERT INTO `teachers` VALUES ('T001', '陈老师');
INSERT INTO `teachers` VALUES ('T002', '刘老师');
INSERT INTO `teachers` VALUES ('T003', '赵老师');

-- ----------------------------
-- Table structure for teachers_students
-- ----------------------------
DROP TABLE IF EXISTS `teachers_students`;
CREATE TABLE `teachers_students`  (
  `sid` int(0) NOT NULL,
  `tid` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`sid`, `tid`) USING BTREE,
  INDEX `FK6nah50rmos7gmmewgql0pe7gh`(`tid`) USING BTREE,
  CONSTRAINT `FK6nah50rmos7gmmewgql0pe7gh` FOREIGN KEY (`tid`) REFERENCES `teachers` (`tid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKnsv9j0siliuhh9gpisvgggrpx` FOREIGN KEY (`sid`) REFERENCES `students` (`sid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of teachers_students
-- ----------------------------
INSERT INTO `teachers_students` VALUES (6, 'T001');
INSERT INTO `teachers_students` VALUES (7, 'T001');
INSERT INTO `teachers_students` VALUES (6, 'T002');
INSERT INTO `teachers_students` VALUES (7, 'T002');
INSERT INTO `teachers_students` VALUES (8, 'T002');
INSERT INTO `teachers_students` VALUES (6, 'T003');
INSERT INTO `teachers_students` VALUES (8, 'T003');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `uid` int(0) NOT NULL AUTO_INCREMENT,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
