package com.simoniu.spt.course.repository;

import com.simoniu.spt.course.entity.Students;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentsRepository extends JpaRepository<Students,Integer> {

}
