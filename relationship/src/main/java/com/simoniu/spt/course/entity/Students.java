package com.simoniu.spt.course.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity //表示这个类对应数据库中的一张表
public class Students implements Serializable,Cloneable {

    //既然是表，就必须有主键
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //使用数据库默认的主键生成策略

    private int sid; //主键
    @Column(length = 20)  //规定学生的姓名不超过20个字符
    private String sname; //姓名，默认是占用255个字符。
    @Column(length = 2)
    private String gender; //性别
    @Column(length = 32)
    private String date; //出生日期
    private String school; //所在学校
    private String major; //所学的专业
    private boolean status; //状态  1：在籍  0：辍学

    @ManyToOne
    @JoinColumn(name="cid",referencedColumnName="cid")
    private ClassRoom classRoom; //学生持有班级的引用。


    //学生持有教师的集合
    @ManyToMany
    //通过中间表来维护多多的关系。表名叫teachers_students
    @JoinTable(name = "teachers_students",
            joinColumns = {
                    @JoinColumn(name = "sid") },
            inverseJoinColumns = {@JoinColumn(name = "tid") }
    )
    private Set<Teachers> teachers; //学生持有的教师集合
}
