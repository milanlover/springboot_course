package com.simoniu.spt.course.service.impl;

import com.simoniu.spt.course.entity.Person;
import com.simoniu.spt.course.repository.PersonRepository;
import com.simoniu.spt.course.service.PersonService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class PersonServiceImpl implements PersonService {

    @Resource
    private PersonRepository personRepository;

    @Override
    public Person savePerson(Person person) {
        return personRepository.save(person);
    }
}
