package com.simoniu.spt.course.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.simoniu.spt.course.entity.Users;

public interface UsersMapper extends BaseMapper<Users> {

}
