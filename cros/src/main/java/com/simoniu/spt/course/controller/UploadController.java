package com.simoniu.spt.course.controller;

import com.simoniu.spt.course.config.UploadPathConfiguration;
import com.simoniu.spt.course.util.R;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;

@RestController
@RequestMapping("upload")
public class UploadController {

    @Resource
    private UploadPathConfiguration uploadPathConfiguration;

    @GetMapping("/")
    //执行文件上传
    public R doUpload(String fileName) {

        System.out.println("用户文件的上传路径是：" + uploadPathConfiguration.getUserFileFile());
        System.out.println("系统文件的上传路径是：" + uploadPathConfiguration.getSystemFilePath());
        System.out.println("日志文件的上传路径是：" + uploadPathConfiguration.getLogFilePath());
        //表示上传成功。
        return R.ok("uploadSuccess");
    }
}