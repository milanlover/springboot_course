package com.simoniu.spt.course.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity //表示这个类对应数据库中的一张表
public class Students implements Serializable,Cloneable {

    //自动递增主键生成策略
    //既然是表，就必须有主键
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //使用数据库默认的主键生成策略
    private Integer sid; //主键


    //手工赋值主键策略
    /*
    @Id
    @GeneratedValue(generator="sid")
    @GenericGenerator(name="sid",strategy="assigned") //表示该主键手工赋值。
    @Column(length = 4)
    private String sid;
    */

    @Column(length = 20)  //规定学生的姓名不超过20个字符
    private String sname; //姓名，默认是占用255个字符。
    @Column(length = 2)
    private String gender; //性别
    @Column(length = 32)
    private String birthday; //出生日期
    private String school; //所在学校
    private String major; //所学的专业
    private boolean status; //状态  1：在籍  0：辍学
}
