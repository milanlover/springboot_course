package com.simoniu.spt.course.service.impl;

import com.simoniu.spt.course.entity.Catalog;
import com.simoniu.spt.course.repository.CatalogRepository;
import com.simoniu.spt.course.service.CatalogService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CatalogServiceImpl  extends BaseServiceImpl<Catalog,Integer, CatalogRepository> implements CatalogService {

    @Override
    public List<Catalog> queryAllCatalogList() {
        return super.findAll();
    }
}
