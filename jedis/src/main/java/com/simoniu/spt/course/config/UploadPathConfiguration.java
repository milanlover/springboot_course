package com.simoniu.spt.course.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "upload")
public class UploadPathConfiguration {

    private String userFileFile;
    private String systemFilePath;
    private String logFilePath;
}
