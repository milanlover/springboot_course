package com.simoniu.spt.course.service.impl;

import com.simoniu.spt.course.entity.Teachers;
import com.simoniu.spt.course.repository.TeachersRepository;
import com.simoniu.spt.course.service.TeachersService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class TeachersServiceImpl implements TeachersService {

    @Resource
    private TeachersRepository teachersRepository;

    @Override
    public Teachers saveTeachers(Teachers teachers) {
        return teachersRepository.save(teachers);
    }
}
