package com.simoniu.spt.course.util;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;

/**
 * Created by Administrator on 2018/1/25.
 */
@Component
public class RedisUtil {

    @Resource
    private RedisTemplate<String, Object> redisTemplate;
    /**
     * 往redis中缓存数据
     * @param key
     * @param object
     * @return
     */
    public boolean set(String key, Object object){
        ValueOperations<String, Object> vo = redisTemplate.opsForValue();
        vo.set(key, object);
        return true;
    }

    /**
     * 根据key从redis服务器中获取value值
     * @param key
     * @return
     */
    public Object get(String key){
        ValueOperations<String, Object> vo = redisTemplate.opsForValue();
        return vo.get(key);
    }

    /**
     * 根据key从Redis中删除value值
     */
    public void remove(String key){
        redisTemplate.delete(key);
    }
}
