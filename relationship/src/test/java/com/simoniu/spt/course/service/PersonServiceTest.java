package com.simoniu.spt.course.service;

import com.simoniu.spt.course.entity.IdCard;
import com.simoniu.spt.course.entity.Person;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PersonServiceTest {

    @Resource
    private PersonService personService;
    @Resource
    private IdCardService idCardService;

    //测试一下居民和身份证之间的一对一的关联关系
    @Test
    public void testSavePerson(){

        //生成一个身份证对象
        IdCard card = new IdCard();
        card.setPid("610103199910103657"); //令狐冲的身份证号码
        card.setProvince("陕西省");

        //生成一个居民的对象
        Person p = new Person();
        p.setName("令狐冲");
        p.setGender("男");
        p.setCard(card); //一样就体现出一对一的关联关系了。

        //先存身份证，再保存居民资料
        idCardService.saveIdCard(card);
        personService.savePerson(p);
    }
}
