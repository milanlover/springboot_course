package com.simoniu.spt.course.config;

import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket applicationApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(applicationInfo())
                .select()
                //swagger要扫描的包路径
                .apis(RequestHandlerSelectors.basePackage("com.simoniu.spt.course.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo applicationInfo() {
        return  new ApiInfoBuilder()
                .title("你的网站标题")
                .description("你的网站描述")
                .termsOfServiceUrl("localhost:8888/myprojectdemo")
                .contact(new Contact("Swagger测试","localhost:8888/myprojectdemo/swagger-ui.html","403353606@qq.com"))
                .version("1.0")
                .build();
    }
}
