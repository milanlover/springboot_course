package com.simoniu.spt.course.entity;

//居民类
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
//居民类应该会映射为一张居民表
public class Person implements Serializable,Cloneable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id; //注意，这是公民表的主键，不是身份证号码
    private String name; //姓名
    private String gender; //性别

    @OneToOne
    @JoinColumn(name="pid",referencedColumnName="pid")
    private IdCard card;

}
