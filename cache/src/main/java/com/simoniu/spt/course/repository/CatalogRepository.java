package com.simoniu.spt.course.repository;

import com.simoniu.spt.course.entity.Catalog;

public interface CatalogRepository extends BaseRepository<Catalog,Integer>{
}
